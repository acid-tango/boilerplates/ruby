# frozen_string_literal: true

class Character
  @@max_health = 1000

  private_class_method :new

  def self.born
    new(@@max_health)
  end

  def initialize(health)
    @health = health
  end

  def alive?
    @health > 0
  end
end
