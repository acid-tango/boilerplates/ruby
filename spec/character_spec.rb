# frozen_string_literal: true

require './app/character'

describe Character do
  it 'starts alive' do
    character = Character.born

    expect(character.alive?).to be true
  end
end
